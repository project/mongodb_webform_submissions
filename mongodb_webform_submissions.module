<?php
/**
 * @file
 * Module file for mongodb_webform_submissions module.
 */

/**
 * Implements hook_ctools_plugin_api().
 *
 * @param string $module
 * @param string $api
 * @return multitype:number
 */
function mongodb_webform_submissions_ctools_plugin_api($module, $api) {
  if ($module == 'webform' && $api == 'plugins') {
    return array('version' => 3);
  }
}

/**
 * Implements hook_webform_plugins().
 */
function mongodb_webform_submissions_webform_plugins() {
  $plugins = array();
  $plugins = array();
  $plugins['WebformSubmissionControllerMongodb'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'mongodb_webform_submissions') . '/plugins',
      'file' => 'WebformSubmissionControllerMongodb.inc',
      'class' => 'WebformSubmissionControllerMongodb',
    ),
  );
  $plugins['WebformSubmissionControllerMongodbGridAnalysis'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'mongodb_webform_submissions') . '/plugins',
      'file' => 'WebformSubmissionControllerMongodbGridAnalysis.inc',
      'class' => 'WebformSubmissionControllerMongodbGridAnalysis',
      'parent' => 'WebformSubmissionControllerMongodb',
    ),
  );
  $plugins['WebformSubmissionControllerMongodbSelectAnalysis'] = array(
    'handler' => array(
      'path' => drupal_get_path('module', 'mongodb_webform_submissions') . '/plugins',
      'file' => 'WebformSubmissionControllerMongodbSelectAnalysis.inc',
      'class' => 'WebformSubmissionControllerMongodbSelectAnalysis',
      'parent' => 'WebformSubmissionControllerMongodb',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_webform_registry_alter().
 *
 * @param array $registry
 */
function mongodb_webform_submissions_webform_registry_alter(&$registry) {
  $registry['controllers']['submission'] = array(
    'title' => t('MongoDB'),
    'plugin' => 'WebformSubmissionControllerMongodb',
  );
  $registry['controllers']['grid_analysis'] = array(
    'title' => t('MongoDB'),
    'plugin' => 'WebformSubmissionControllerMongodbGridAnalysis',
  );
  $registry['controllers']['select_analysis'] = array(
    'title' => t('MongoDB'),
    'plugin' => 'WebformSubmissionControllerMongodbSelectAnalysis',
  );
}

/**
 * Migrate webform submissions from MySQL to Mongodb.
 */
function mongodb_webform_submissions_migrate_to_mongodb() {
  module_load_include('inc', 'webform', 'plugins/WebformSubmissionControllerMysql');
  module_load_include('inc', 'mongodb_webform_submissions', 'plugins/WebformSubmissionControllerMongodb');

  $sql_storage_controller = new WebformSubmissionControllerMysql();
  $mongo_storage_controller = new WebformSubmissionControllerMongodb();

  $i = 0;
  while ($submissions = $sql_storage_controller->getSubmissions(array(), NULL, 0, 1000)) {
    foreach ($submissions as $submission) {
      $i++;
      $node = new stdClass();
      $node->nid = $submission->nid;
      $node->webform['nid'] = $submission->nid;
      foreach (array('sid', 'submitted', 'nid', 'uid', 'is_draft') as $prop) {
        $submission->$prop = (int) $submission->$prop;
      }
      $mongo_storage_controller->insertSubmission($node, $submission);
      $sql_storage_controller->deleteSubmissions($node, $submission);
    }
  }
  drupal_set_message(t('Migrated @n webform submissions from MySQL to MongoDB.', array('@n' => $i)));
}

/**
 * Migrate webform submissions from MongoDB to MySQL.
 */
function mongodb_webform_submissions_migrate_to_db() {
  module_load_include('inc', 'webform', 'plugins/WebformSubmissionControllerMysql');
  module_load_include('inc', 'mongodb_webform_submissions', 'plugins/WebformSubmissionControllerMongodb');

  $sql_storage_controller = new WebformSubmissionControllerMysql();
  $mongo_storage_controller = new WebformSubmissionControllerMongodb();

  $i = 0;
  while ($submissions = $mongo_storage_controller->getSubmissions(array(), NULL, 0, 1000)) {
    foreach ($submissions as $submission) {
      $i++;
      $node = new stdClass();
      $node->nid = $submission->nid;
      $node->webform['nid'] = $submission->nid;
      $sql_storage_controller->insertSubmission($node, $submission);
      $mongo_storage_controller->deleteSubmissions($node, $submission);
    }
  }
  drupal_set_message(t('Migrated @n webform submissions from MongoDB to MySQL.', array('@n' => $i)));
}
